# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.0](https://github.com/TinkoffCreditSystems/tui-ngrx-forms-adapter/compare/v1.0.2...v1.2.0) (2021-12-15)

### Features

-   **all:** change lib name ([e6f3a2c](https://github.com/TinkoffCreditSystems/tui-ngrx-forms-adapter/commit/e6f3a2cfb7222518610e3e2341f24c1f51ac3ce4))

## [1.1.0](https://github.com/TinkoffCreditSystems/tui-ngrx-forms-adapter/compare/v1.0.2...v1.1.0) (2021-12-15)

### Features

-   **all:** change lib name ([e6f3a2c](https://github.com/TinkoffCreditSystems/tui-ngrx-forms-adapter/commit/e6f3a2cfb7222518610e3e2341f24c1f51ac3ce4))

### [1.0.1](https://github.com/TinkoffCreditSystems/tui-ngrx-forms-adapter/compare/v1.0.2...v1.0.1) (2021-12-15)
